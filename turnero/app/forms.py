from . import models
from django import forms


class CargaDatosClienteForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["nombre"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["apellido"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["email"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["nro_documento"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["nro_telefono"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["fecha_nacimiento"].widget.attrs.update({
            'class':'form-control'
        })
    class Meta:
        model= models.Cliente
        fields= ["nro_documento","nombre","apellido","email","nro_telefono","fecha_nacimiento"]

        widgets ={
            'fecha_nacimiento': forms.DateInput(attrs={'type':'date'}),
        }


class TurnoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["cliente"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["cola"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["prioridad"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["estado"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["fecha_ticket"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["hora_ticket"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["estado"].queryset=models.Estado.objects.filter(estado_descripcion="En Espera") 

    class Meta:
        model= models.Turno_ticket
        fields = ["cliente","cola","prioridad","estado", "fecha_ticket", "hora_ticket"]

    
class ActualizarTurnoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["cliente"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["cola"].widget.attrs.update({
            'class':'form-select border-warning'
        })
        self.fields["prioridad"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["estado"].widget.attrs.update({
            'class':'form-select border-warning'
        })
        self.fields["fecha_ticket"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["hora_ticket"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["numero_turno"].widget.attrs.update({
            'class':'form-control'
        })
        self.fields["cliente"].widget.attrs['readonly'] = True
        self.fields["numero_turno"].widget.attrs['readonly'] = True
        self.fields["hora_ticket"].widget.attrs['readonly'] = True
        self.fields["fecha_ticket"].widget.attrs['readonly'] = True
        self.fields["prioridad"].widget.attrs['readonly'] = True
        self.fields['cola'].label = "Servicio"
        self.fields["estado"].queryset=models.Estado.objects.exclude(
                                estado_descripcion="En Turno").exclude(
                                estado_descripcion="Abandonado"
                                ).exclude(estado_descripcion="Atendido")

    class Meta:
        model= models.Turno_ticket
        fields = ["turno_id","cliente","cola","prioridad","estado", "numero_turno", "fecha_ticket", "hora_ticket"]

  


# class ColaForm(forms.ModelForm):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)

#     class Meta:
#         model= models.Cola
#         fields = ["servicio","prioridad"]
        
   

