"""turnero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app import views

from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    path('admin/', admin.site.urls),
    path('carga_datos_cliente/', views.carga_datos_cliente, name="carga_datos_cliente"),
    path('inicio/', views.inicio, name="inicio"),
    path('otorgar_turno/<str:pk>', views.crear_turno, name="otorgar_turno"),
    path('busqueda_cliente/', views.buscar_cliente, name="busqueda_cliente"),
    path('ver_cola/<str:servicio>', views.ver_cola, name="ver_cola"),
    path('atender/<str:servicio><int:n>', views.atender, name="atender"),
    path('filtrar_servicios/<str:accion>', views.filtar_servicios, name="filtrar_servicios"),
    path('cancelar_o_derivar/', views.cancelar_o_derivar, name="cancelar_o_derivar"),
    path('',views.login_usuario, name='login'),
    path('cerrar/',views.cerrar, name='cerrar'),

]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)

